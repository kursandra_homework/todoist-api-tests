# todoist-api-tests

This is a little test framework with tests for todoist API.

For preparation use: ```npm install```

For run tests use: ```npm test```
