import { describe, expect, test } from '@jest/globals';
import log4js from 'log4js';
import { user, urls } from '../framework/config';
import { Projects } from '../framework/services';

const logger = log4js.getLogger();
logger.level = 'info';

beforeEach(() => {
  logger.info('____________________');
  logger.info('---- Start test ----');
});
describe('Projects', () => {
  const projects = new Projects({ logger, url: urls.todoist, token: user.todoist.access_token });
  const projectsWithoutToken = new Projects({ logger, url: urls.todoist });

  describe('Get projects', () => {
    test('Test-1: Get all projects happy pass', async () => {
      const response = await projects.getAll();
      expect(response.status).toEqual(200);
    });

    test('Test-2: Get all projects without token', async () => {
      const response = await projectsWithoutToken.getAll();
      expect(response.status).toEqual(403);
    });
  });

  describe('Get one project', () => {
    test('Test-3: Get project by id', async () => {
      logger.info('Precondition: project creation');
      const project = await projects.create('any project');
      const response = await projects.getOne(project.body.id);
      expect(response.status).toEqual(200);
      expect(response.body.name).toEqual('any project');
      logger.info('Cleanup: project deletion');
      await projects.delete(project.body.id);
    });

    test('Test-4: Get project without id', async () => {
      const response = await projects.getOne();
      expect(response.status).toEqual(404);
    });
  });

  describe('Create project', () => {
    test('Test-5: Create new project with name only', async () => {
      logger.info('Precondition: project creation');
      const response = await projects.create('Shopping List');
      expect(response.status).toEqual(200);
      expect(response.body.name).toEqual('Shopping List');
      logger.info('Cleanup: project deletion');
      await projects.delete(response.body.id);
    });

    test('Test-6: Create new project without name', async () => {
      const response = await projects.create();
      expect(response.status).toEqual(400);
    });
  });

  describe('Delete project', () => {
    test('Test-7: Delete project by id', async () => {
      logger.info('Precondition: project creation');
      const project = await projects.create('New project');
      const response = await projects.delete(project.body.id);
      expect(response.status).toEqual(204);
    });

    test('Test-8: Delete project without id', async () => {
      const response = await projects.delete();
      expect(response.status).toEqual(404);
    });
  });

  describe('Update project', () => {
    test('Test-9: Update project name', async () => {
      logger.info('Precondition: project creation');
      const project = await projects.create('New project');
      const response = await projects.update(project.body.id, 'new name');
      expect(response.status).toEqual(204);
      logger.info('Cleanup: project deletion');
      await projects.delete(project.body.id);
    });

    test('Test-10: Update project without token', async () => {
      logger.info('Precondition: project creation');
      const project = await projects.create('New project');
      const response = await projectsWithoutToken.update(project.body.id, 'new name');
      expect(response.status).toEqual(403);
      logger.info('Cleanup: project deletion');
      await projects.delete(project.body.id);
    });
  });
});
