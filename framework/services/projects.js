import supertest from 'supertest';
import log4js from 'log4js';

const logger = log4js.getLogger();
logger.level = 'info';

class Projects {
  constructor({ logger, url, token }) {
    this.url = url;
    this.token = token;
    this.logger = logger;
  }

  async getAll() {
    this.logger.info('Get all projects');
    return supertest(this.url)
      .get('/rest/v1/projects')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${this.token}`);
  }

  async getOne(id) {
    this.logger.info(`Get project by id:${id}`);
    return supertest(this.url)
      .get(`/rest/v1/projects/${id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${this.token}`);
  }

  async create(name) {
    this.logger.info(`Create project with name: ${name}`);
    return supertest(this.url)
      .post('/rest/v1/projects')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${this.token}`)
      .send({ name });
  }

  async delete(id) {
    this.logger.info(`Delete project with id:${id}`);
    return supertest(this.url)
      .delete(`/rest/v1/projects/${id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${this.token}`);
  }

  async update(id, name) {
    this.logger.info(`Update name of the project by id:${id}, new name:${name}`);
    return supertest(this.url)
      .post(`/rest/v1/projects/${id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${this.token}`)
      .send({ name });
  }
}

export { Projects };
