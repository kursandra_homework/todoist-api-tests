require('dotenv').config();

const user = {
  todoist: {
    access_token: process.env.TOKEN,
  },
};
export { user };
